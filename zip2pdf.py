#! /usr/bin/env python

from pathlib import Path
from PyPDF2 import PdfFileMerger
from zipfile import ZipFile

import sys

def pdf_merge(archive, output_path=None):
    if output_path is None:
        input_path = Path(archive.filename)
        output_path = input_path.with_suffix('.pdf')
        
    merger = PdfFileMerger()
    for file in archive.namelist():
        if not file.endswith(".pdf"):
            continue
        with (archive.open(file, mode='r')) as pdf:
            merger.append(pdf)
    with open(output_path, "wb") as pdf_file:
        merger.write(pdf_file)
    return output_path


if __name__ == "__main__":        
    if len(sys.argv) <= 1:
        print(f"Usage: {sys.argv[0]} <zipfile>")
        print(f"   or: {sys.argv[0]} \"<zipfile>\"")
        sys.exit(2) # command line error

    zippath = Path(sys.argv[1])
    if not zippath.exists():
        print(f"Error: File {zippath.name} not found!")
        sys.exit(1)

    z = ZipFile(zippath)
    if output_path:= pdf_merge(z):
        print(f"PDF content written to '{output_path.name}'.")
    

