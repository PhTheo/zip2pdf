# Purpose

[EvaExam](https://www.evasys.co.za/products/evaexam/) in the version used at TH Köln 
exports examination sheets in seperate [PDF](https://en.wikipedia.org/wiki/PDF) files, 
that can be downloaded in a single [zipfile](https://en.wikipedia.org/wiki/ZIP_(file_format)).

This Python script takes such a zipfile and creates a
single PDF file with all the PDF content from the input file.

# Usage

Usage: `zip2pdf <zipfile>`

